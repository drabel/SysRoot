# 6UL配置板载系统注意  

## Config-A   
默认的用带Qt5的版本，里面包含了Qt4的版本，使用MfgTools里面的配置文件进行烧录。    
首先  
1. 拷贝 OS Firmware_rootfs\image-linux-31452\distro-fsl-imx-fb-image-qt5.tar.bz2 到 MfgTools\Profiles\Linux\OS Firmware 。
2. 拷贝 OS Firmware_rootfs\image-linux-31452\distro-fsl-image.tar.bz2 到 MfgTools\Profiles\Linux\OS Firmware 。
2. 拷贝 OS Firmware_rootfs\image-linux-31452\distro-fsl-image-6ul.tar.bz2 到 MfgTools\Profiles\Linux\OS Firmware 。

U盘初始化   
这个ROM需要使用U盘根目录的数据初始化。    
拷贝所有文件到U盘根目录  
1.install.sh 允许多次调用  
2.fix-once.sh 只允许调用一次  

这个版本，myzr对Qt目录做了很多处理，Qt4的lib移植到/lib下，Qt5的lib移植到/usr/lib下，而且Qt5的其他目录都安置到了/usr/xx/qt5里面，实在不方便使用。  
这个配置方式，建议开发者使用。思路简单，MyZR的写入工具，配合U盘初始化，对MyZR改动很小。  

## Config-B   
这个配置修改了myzr配置文件，使用6ul基础文件系统，并且使用U盘进行初始化。定制程度比较高，思路顺，建议2级开发者使用。研究下MYZR。    

## Config-C   
直接烧录应用程序，使用MfgTools里面的配置文件进行烧录，一次烧录完成不用其他初始化，直接能够开机运行。  
建议批量生产时使用。   

[返回](.)  
