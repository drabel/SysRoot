
准备文件：
1. 需要从网盘下载的文件：my-imx6-mfgtool-lib262.rar、OS Firmware 目录、OS Firmware_rootfs 目录


操作步骤：
1. 解压 my-imx6-mfgtool-lib262.rar 到 Windows，得到 my-imx6-mfgtool-lib262。

2. 把 OS Firmware 目录下的文件及文件夹复制到 my-imx6-mfgtool-lib262\Profiles\Linux\OS Firmware 目录下。

3. 把 OS Firmware_rootfs 目录下的文件夹复制到 my-imx6-mfgtool-lib262\Profiles\Linux\OS Firmware 目录下。

4. 运行 MfgConfig.exe，按自己烧录的需要选择对应的配置后，点击"Make"以生成或更新烧录配置文件。

5. 运行 MfgTool2.exe 。


说明：
1）my-imx6-mfgtool-lib262.rar 主要是烧录工具主程序。
2）OS Firmware 目录下 firmware 开头的文件夹是烧录工具需要使用的文件（firmware 是烧录工具用的）。
3）OS Firmware 目录下 image 开头的文件夹是烧录到评估板中的文件（image 是烧录到评估板的）。
4）OS Firmware_rootfs 目录是文件系统包，需要把内部的相应rootfs目录拷贝到Profile/Linux/OS Firmware里。
5）MfgConfig.exe 是烧录工具的配置程序。
6）MfgTool2.exe 是烧录工具主程序。
7）更多内容请参考“http://wiki.myzr.com.cn/” -> 《MY-IMX6 开发板 MfgTool 烧录指导》


其它：
my-imx6-mfgtool-lib262 可以支持多达 7 个 MY-IMX6 板子同时烧录（需要将 UICfg.ini 中的 “PortMgrDlg=1” 改为 “PortMgrDlg=7”）。
