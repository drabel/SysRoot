#!/bin/bash
cd /
#create some link
cd /usr/lib
ln -s ../../lib/libudev.so.0 libudev.so.0
ln -s ../../lib/libcrypto.so.1.0.0 libcrypto.so.1.0.0
ln -s ../../lib/libcrypto.so.1.0 libcrypto.so.1.0
ln -s ../../lib/libcrypto.so.1 libcrypto.so.1
ln -s ../../lib/libcrypto.so libcrypto.so

USB_MOUNT=/mnt/usb0
cd ${USB_MOUNT}

#install Qt 4.8.5 and 5.5.1
QTDIR=/usr/local/Qt/4.8.5
mkdir -p ${QTDIR}
tar xzvf qt4.8.5.tar.gz -C ${QTDIR}
QTDIR=/usr/local/Qt/5.5.1
mkdir -p ${QTDIR}
tar xzvf qt5.5.1.tar.gz -C ${QTDIR}

#install iconv
tar xzvf iconv.tar.gz -C /usr
#install opengl openal
tar xzvf opengl.tar.gz -C /usr
#install ssh
#tar xzvf zlib1.2.11.tar.gz -C /usr
#tar xzvf ssl1.0.2n.tar.gz -C /usr
tar xzvf ssh6.4.tar.gz -C /usr/local

#remove Qt4 from myzr dir
rm -fr /usr/local/Trolltech
rm -fr /lib/libQt*
#remove Qt5 from myzr dir
rm -fr /usr/bin/qt5
rm -fr /usr/include/qt5
rm -fr /usr/lib/qt5
rm -fr /usr/share/qt*
rm -fr /usr/lib/libQt5*

tar xzvf rootfs-fix.tar.gz -C /

reboot
