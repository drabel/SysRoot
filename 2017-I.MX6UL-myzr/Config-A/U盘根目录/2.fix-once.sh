#!/bin/bash


mkdir -p /usr/local/bin/ 
mkdir -p /usr/local/sbin/ 
mkdir -p /usr/local/etc/ 
mkdir -p /usr/local/libexec/ 
mkdir -p /var/run/ 
mkdir -p /var/empty/

cd /usr/local/etc
ssh-keygen -t rsa -f ssh_host_rsa_key -N ""
ssh-keygen -t dsa -f ssh_host_dsa_key -N ""
ssh-keygen -t ecdsa -f ssh_host_ecdsa_key -N "" 

#在/etc/passwd 中添加下面这一行 
#sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
echo sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin >> /etc/passwd
/usr/local/sbin/sshd
