#!/bin/bash

#tslib settings
export TSLIB_ROOT=/usr/local/6ul-tslib
export TSLIB_CONSOLEDEVICE=none
export TSLIB_FBDEVICE=/dev/fb0
export TSLIB_TSDEVICE=/dev/input/touchscreen0
export TSLIB_CONFFILE=/etc/ts.conf
export TSLIB_PLUGINDIR=$TSLIB_ROOT/lib/ts

#qt settings
export QTDIR=/usr/local/Qt/4.8.5
export QPEDIR=$QTDIR
export PATH=/Application:$QTDIR/bin:$PATH
export QT_QWS_FONTDIR=$QTDIR/lib/fonts
export QWS_MOUSE_PROTO=tslib:/dev/input/touchscreen0
#export QWS_KEYBOARD=USB:/dev/input/event2
export QWS_DISPLAY=LinuxFb:/dev/fb0

export PATH=/Application:$QTDIR/bin:$PATH
export LD_LIBRARY_PATH=/Application:$QTDIR/lib:$LD_LIBRARY_PATH
export LD_PRELOAD=/usr/lib/preloadable_libiconv.so:$TSLIB_ROOT/lib/libts.so

cd /Application
/Application/$1 -qws &
