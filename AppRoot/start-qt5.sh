#!/bin/bash

#tslib settings
export TSLIB_ROOT=/usr/local/6ul-tslib
export TSLIB_CONSOLEDEVICE=none
export TSLIB_FBDEVICE=/dev/fb0
export TSLIB_TSDEVICE=/dev/input/touchscreen0
export TSLIB_CONFFILE=/etc/ts.conf
export TSLIB_PLUGINDIR=$TSLIB_ROOT/lib/ts

#qt5 setting
export QTDIR=/usr/local/Qt/5.5.1
export QPEDIR=$QTDIR
export QT_QPA_PLATFORM_PLUGIN_PATH=$QTDIR/plugins
export QT_QPA_PLATFORM=linuxfb:tty=/dev/fb0
export QT_QPA_FONTDIR=$QTDIR/lib/fonts
export QT_QPA_GENERIC_PLUGINS=tslib

#path
export PATH=/Application:$QTDIR/bin:$PATH
export LD_LIBRARY_PATH=/Application:$QTDIR/lib:$LD_LIBRARY_PATH
export LD_PRELOAD=/usr/lib/preloadable_libiconv.so:$TSLIB_ROOT/lib/libts.so

cd /Application
/Application/$1 -platform linuxfb &